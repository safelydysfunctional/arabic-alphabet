export default [
  {
    "letter": "\u0623",
    "sound": "a",
    "name": "alif"
  }, {
    "letter": "\u0628",
    "sound": "b",
    "name": "baa"
  }, {
    "letter": "\u062A",
    "sound": "t",
    "name": "taa"
  }, {
    "letter": "\u062B",
    "sound": "th",
    "name": "thaa"
  }, {
    "letter": "\u062C",
    "sound": "j",
    "name": "jiim"
  }
]

