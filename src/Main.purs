module Main where

import Prelude

import Data.Tuple (Tuple(..))
import Data.Maybe (Maybe(..))
import Data.Array (unsafeIndex, length)

import Partial.Unsafe (unsafePartial)

import Effect (Effect)
import Effect.Console (log)
import Effect.Class (liftEffect)
import Effect.Random (randomInt)
import Effect.Aff (Aff)

import Flame (Html, QuerySelector(..), Subscription)
import Flame.Application.Effectful (AffUpdate)
import Flame.Application.Effectful as FE
import Flame.Html.Element as HE
import Flame.Html.Event as HEv
import Flame.Html.Attribute as HA


type ArabicLetterFFI = { letter :: String
                       , sound :: String
                       , name :: String
                       }

foreign import letters :: Array ArabicLetterFFI

data LetterPosition = Start | End | Middle | Loose

type AppState = { currentLetter :: ArabicLetterFFI
                , currentLetterIndex :: Int
                , position :: LetterPosition
                , error :: Maybe String
                , userInput :: String
                }

type Model = Maybe AppState

data Message = NextLetter | SetInput String | Guess | Noop

init :: Tuple Model (Maybe Message)
init = Tuple Nothing (Just NextLetter)

chooseRandom :: forall a. Array a -> Effect a
chooseRandom list = do
  r <- randomInt 0 (length list - 1)
  pure <<< unsafePartial $ unsafeIndex list r

randomIntExcept :: Int -> Int -> Maybe Int -> Effect Int
randomIntExcept min max Nothing = randomInt min max
randomIntExcept min max (Just a) = do
  r <- randomInt min max

  if a == r
    then randomIntExcept min max (Just a)
    else pure r


nextLetter :: Model -> Aff (Model -> Model)
nextLetter model = liftEffect $ do
  let currentIndex = case model of
        Nothing -> Nothing
        Just m -> Just m.currentLetterIndex
      
  nextIndex <- randomIntExcept 0 (length letters - 1) currentIndex
  nextPosition <- chooseRandom [Start, End, Middle, Loose]

  let
    letter = unsafePartial $ unsafeIndex letters nextIndex
    newModel =
      const $ Just { currentLetter: letter
                   , currentLetterIndex: nextIndex
                   , position: nextPosition
                   , error: Nothing
                   , userInput: ""
                   }

  pure newModel

update :: AffUpdate Model Message
update { model: Nothing } = nextLetter Nothing
update { model: Just model, message } =
  case message of
    NextLetter -> nextLetter (Just model)

    SetInput input -> do
      liftEffect $ log input
      pure $ map (_ { userInput = input, error = Nothing })

    Guess ->
      if model.userInput == model.currentLetter.sound
      then nextLetter (Just model)
      else pure $ map (_ { error = Just "O fonema não corresponde a letra"
                         , userInput = ""
                         })

    Noop -> pure identity


letterInContext :: String -> LetterPosition -> String
letterInContext letter Loose = letter
letterInContext letter Start = letter <> "ـ"
letterInContext letter End = "ـ" <> letter
letterInContext letter Middle = "ـ" <> letter <> "ـ"

view :: Model -> Html Message
view Nothing = HE.text ""
view (Just model) =
  let
    viewError Nothing = HE.text ""
    viewError (Just error) =
      HE.div [HA.class' "row"]
      [HE.div [HA.class' "form-text text-danger"] error]

    guessOnEnter (Tuple "Enter" _) = Guess
    guessOnEnter _ = Noop
  in HE.main "main"
     [ HE.p [HA.class' "text-center letter-text"]
       (letterInContext model.currentLetter.letter model.position)
     , HE.div [HA.class' "row"]
       [ HE.div [HA.class' "col-1"]
         [ HE.label [HA.for "sound-input", HA.class' "col-form-label"]
           [HE.strong_ "Som:"]
         ]
       , HE.div [HA.class' "col-8"]
         [ HE.input [ HA.type' "text"
                    , HA.class' "form-control"
                    , HA.id "sound-input"
                    , HA.value model.userInput
                    , HEv.onInput SetInput
                    , HEv.onKeypress guessOnEnter
                    ]
         ]
       , HE.div [HA.class' "col-2 d-grid gap-2"]
         [ HE.button
           [ HA.onClick Guess,
             HA.class' "btn btn-primary"
           ]
           "Tentar"
         ]
       ]
     , viewError model.error 
     ]


subscribe :: Array (Subscription Message)
subscribe = []

main :: Effect Unit
main = do
  FE.mount_ (QuerySelector "#app") { init, view, update, subscribe }
